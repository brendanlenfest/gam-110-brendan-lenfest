﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame

    {
        internal class Maze
        {
            char[,] Tiles =
              {
            {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' },
            {'#','S',' ','#','#','$',' ',' ','#','#',' ',' ','#',' ',' ','#' },
            {'#','#',' ',' ',' ',' ','#',' ',' ','#',' ','#','#',' ','#','#' },
            {'#','#',' ','#','#',' ','#',' ','#',' ',' ',' ',' ',' ',' ','#' },
            {'#',' ',' ',' ','#',' ','#','#','#',' ','#','#',' ','#',' ','#' },
            {'#','#',' ','#',' ',' ',' ','$','#',' ','#',' ',' ','#',' ','#' },
            {'#','#',' ','#','#',' ','#',' ',' ',' ','#','#',' ','#','#','#' },
            {'#',' ',' ',' ','#',' ','#',' ','#',' ','D','#',' ',' ','#','#' },
            {'#','#',' ','#','#',' ','#',' ','#','#',' ','#','#',' ',' ','#' },
            {'#',' ',' ','#',' ',' ','#',' ','#',' ',' ',' ','#','#',' ','#' },
            {'#','#',' ','#','#',' ','#','#','#','#','#',' ','#',' ','#','#' },
            {'#',' ','K',' ','#','#',' ','#',' ',' ','#',' ',' ',' ',' ','#' },
            {'#',' ','#','#',' ','#',' ','#','#',' ','#','#','#',' ','#','#' },
            {'#',' ','#',' ',' ','#',' ',' ','#',' ','#',' ','#',' ',' ','#' },
            {'#',' ','#',' ','#','#','#',' ',' ',' ','#',' ','#',' ','#','#' },
            {'#',' ','#',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','E','#' },
            {'#','$',' ',' ',' ',' ',' ',' ','#',' ','#',' ','#','#','#','#' },
            {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' }
            };
            int xSize;
            int ySize;
            Position Player;
            bool hasKey = false;
            int Gold = 0;

            public Maze()
            {
                Player = new Position(1, 1);
                xSize = Tiles.GetLength(0);
                ySize = Tiles.GetLength(1);
            }

            internal void DisplayMaze()
            {
                Console.CursorVisible = false;
                for (int x = 0; x < xSize; x++)
                {
                    for (int y = 0; y < ySize; y++)
                    {
                        if (Tiles[x, y] == '#')
                        {
                            Console.BackgroundColor = ConsoleColor.Blue;
                        }
                        Console.SetCursorPosition(x, y);
                        Console.Write(Tiles[x, y]);
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                }
                Console.SetCursorPosition(Player.x, Player.y);
                Console.WriteLine('P');
                Console.SetCursorPosition(xSize + 1, 0);
                Console.WriteLine("Gold: " + Gold);
                Console.SetCursorPosition(xSize + 1, 1);
                Console.WriteLine("Key: " + hasKey);
            }

            internal bool NextMove()
            {
                bool keepplaying = true;
                ConsoleKey input = Console.ReadKey().Key;
                Position NewPosition = Player;
                switch (input)
                {
                    case ConsoleKey.Q:
                        keepplaying = false;
                        break;
                    case ConsoleKey.Escape:
                        keepplaying = false;
                        break;
                    case ConsoleKey.UpArrow:
                        NewPosition.MoveUP();
                        break;
                    case ConsoleKey.DownArrow:
                        NewPosition.MoveDown();
                        break;
                    case ConsoleKey.LeftArrow:
                        NewPosition.MoveLeft();
                        break;
                    case ConsoleKey.RightArrow:
                        NewPosition.MoveRight();
                        break;

                    default:
                        break;
                }
                if (IsWalkable(NewPosition))
                {
                    Player = NewPosition;
                }
                if (Tiles[Player.x, Player.y] == '$')
                {
                    Gold++;
                    Tiles[Player.x, Player.y] = ' ';
                }
                if (Tiles[Player.x, Player.y] == 'K')
                {
                    hasKey = true;
                    Tiles[Player.x, Player.y] = ' ';
                }
                if (Tiles[Player.x, Player.y] == 'E')
                {
                    Console.SetCursorPosition(0, ySize + 1);
                    Console.WriteLine("You have won!!");
                }


                return keepplaying;
            }

            private bool IsWalkable(Position p)
            {
                bool isWall = Tiles[p.x, p.y] == '#';
                bool isLockedDoor = Tiles[p.x, p.y] == 'D' && !hasKey;

                return (!isWall && !isLockedDoor);
            }
        }


        struct Position
        {
            public int x;
            public int y;
            public Position(int _x, int _y)
            {
                x = _x;
                y = _y;
            }
            public void MoveUP()
            {
                y--;
            }
            public void MoveDown()
            {
                y++;
            }
            public void MoveLeft()
            {
                x--;
            }
            public void MoveRight()
            {
                x++;
            }
        }
    }
}
    

