﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            int type = 0;
            int str = 0;
            string[] characterTypes = {
                "Accountant",
                "Plumber" ,
                "Alchemist",
                "Student" };

            for (int i = 0; i < characterTypes.Length; i++)
            {
                Console.WriteLine((i + 1) + ") " + characterTypes[i]);
            }

            bool validchoice = false;
            while (!validchoice)
            {
                Console.Write("Pick a type: ");
                string input = Console.ReadLine();
                bool isanumber = int.TryParse(input, out type);
                type--;
                validchoice =
                    isanumber
                    && (type) > -1
                    && (type) < characterTypes.Length;
            }

            Console.WriteLine("You picked: " + characterTypes[type]);

           
            Console.Write("Give me a number (try catch): ");
            string tryParseinput = Console.ReadLine();
            try
            {
                int number = int.Parse(tryParseinput);
                Console.WriteLine(
                    "You typed " + tryParseinput +
                    "\n This is the number " + number +
                    "\n I can add 5 to this " + (number + 5) +
                    "\n Because it is a number it can be formatted like this " + number.ToString("0.00"));
            }
            catch (Exception e)
            {
                Console.WriteLine(tryParseinput + " is not a number and if I tried to add 5 it would break the internet");
            }
        }
    }
}
