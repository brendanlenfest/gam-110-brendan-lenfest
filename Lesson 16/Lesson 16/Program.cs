﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Location P1Loc = new Location(1, 1);
            Location P2Loc = new Location(3, 3);
            Console.WriteLine("Class P1:" + P1Loc.x + " " + P1Loc.y + " P2: " + P2Loc.x + " " + P2Loc.y + " ");

            P1Loc.y++;
            Console.WriteLine("Class P1:" + P1Loc.x + " " + P1Loc.y + " P2: " + P2Loc.x + " " + P2Loc.y + " ");

            P1Loc = P2Loc;
            Console.WriteLine("Class P1:" + P1Loc.x + " " + P1Loc.y + " P2: " + P2Loc.x + " " + P2Loc.y + " ");

            P1Loc.y++;
            Console.WriteLine("Class P1:" + P1Loc.x + " " + P1Loc.y + " P2: " + P2Loc.x + " " + P2Loc.y + " ");

            Position P1Pos = new Position(1, 1);
            Position P2Pos = new Position(3, 3);
            Console.WriteLine("Struct P1:" + P1Pos.x + " " + P1Pos.y + " P2: " + P2Pos.x + " " + P2Pos.y + " ");

            P1Pos.y++;
            Console.WriteLine("Struct P1:" + P1Pos.x + " " + P1Pos.y + " P2: " + P2Pos.x + " " + P2Pos.y + " ");

            P1Pos = P2Pos;
            Console.WriteLine("Struct P1:" + P1Pos.x + " " + P1Pos.y + " P2: " + P2Pos.x + " " + P2Pos.y + " ");

            P1Pos.y++;
            Console.WriteLine("Struct P1:" + P1Pos.x + " " + P1Pos.y + " P2: " + P2Pos.x + " " + P2Pos.y + " ");
        }
    }

    struct Position
    {
        public int x;
        public int y;

        public Position(int _x, int_y)
        {
            x = _x;
            y = _y;
        }
    }
    internal class Location
    {
        public int x;
        public int y;

        public Location(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
