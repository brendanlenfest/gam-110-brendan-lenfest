﻿using System;

namespace Conversion_Thingy
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] unitNames = { "Schmeckles", "Gold", "Diamond", "Rupies", "Bottle Caps" };
            float[] baseUnitConversions = { 1, 10, 100, 1000, 9 };
            float testAmount = 10;
            for (int inUnit = 0; inUnit < 5; inUnit++)
            {
                for (int outUnit = 0; outUnit < 5; outUnit++)
                {
                    float baseUnits = testAmount * baseUnitConversions[inUnit];
                    float outUnits = baseUnits / baseUnitConversions[outUnit];
                    Console.WriteLine
                        (
                        testAmount + " " + unitNames[inUnit] +
                        " is " + baseUnits + " " + unitNames[0] +
                        " which is " + outUnits + " " + unitNames[outUnit]
                        );
                }
            }

           
            foreach (string unitName in unitNames)
            {
                Console.WriteLine("UnitName: " + unitName);
            }

            foreach (float BaseUnitConversion in baseUnitConversions)
            {
                Console.WriteLine("BaseUnitConversion: " + BaseUnitConversion);
            }
        }
    }
}
