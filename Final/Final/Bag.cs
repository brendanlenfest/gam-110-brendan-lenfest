﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bag
    {
        List<string> Items = new List<string>();
        string LastItem;
        public Bag()
        {
            Items.Add("water");
            Items.Add("fire");
            Items.Add("dirt");
            Items.Add("air");
            Items.Add("quit");
        }

        public void DisplayItems()
        {
            Console.Clear();
            Items.Sort();
            int column = 0;
            int row = 0;
            foreach (String item in Items)
            {
                Console.SetCursorPosition(column, row);
                Console.Write("|  " + item);
                column += 15;
                if (column > 61)
                {
                    column = 0;
                    row++;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Last item made: " + LastItem);
        }

        internal string GetItem()
        {
            Console.WriteLine();
            Console.WriteLine("Pick an item: ");
            string input = Console.ReadLine();
            if (Items.Contains(input.ToLower()))
            {
                return input;
            }
            return "nothing";
        }

        internal void AddItem(string item)
        {
            if (!Items.Contains(item.ToLower()))
            {
                Items.Add(item);
            }
            LastItem = item;
        }
    }
}
