﻿
using System.Collections.Generic;

namespace ConsoleApp1
{
    internal class CraftingTable
    {
        List<Recipe> recipes = new List<Recipe>();

        public CraftingTable()
        {
            recipes.Add(new Recipe("water", "fire", "steam"));
            recipes.Add(new Recipe("fire", "fire", "anger"));
            recipes.Add(new Recipe("water", "dirt", "mud"));
            recipes.Add(new Recipe("water", "water", "storm"));
            recipes.Add(new Recipe("air", "air", "wind"));
            recipes.Add(new Recipe("water", "wind", "ice"));
            recipes.Add(new Recipe("ice", "ice", "baby"));
            recipes.Add(new Recipe("air", "air", "pressure"));
            recipes.Add(new Recipe("pressure", "dirt", "rock"));
            recipes.Add(new Recipe("wind", "rock", "sand"));
            recipes.Add(new Recipe("sand", "fire", "glass"));
            recipes.Add(new Recipe("glass", "light", "laser"));
            recipes.Add(new Recipe("water", "dirt", "mud"));
            recipes.Add(new Recipe("water", "nothing", "mist"));
            recipes.Add(new Recipe("nothing", "nothing", "nothing"));
            recipes.Add(new Recipe("quit", "quit", "really quit"));
            recipes.Add(new Recipe("quit", "nothing", "half quit"));
            recipes.Add(new Recipe("half quit", "half quit", "full quit"));
        }
        public string Combine(string item1, string item2)
        {
            foreach (Recipe recipe in recipes)
            {
                if (recipe.Contains(item1, item2))
                {
                    return recipe.result;
                }
            }
            return "mistake";
        }
    }
    struct Recipe
    {
        public string item1;
        public string item2;
        public string result;
        public Recipe(string i1, string i2, string r)
        {
            item1 = i1;
            item2 = i2;
            result = r;
        }
        public bool Contains(string i1, string i2)
        {
            return (i1 == item1 && i2 == item2) || (i2 == item1 && i1 == item2);
        }
    }
}