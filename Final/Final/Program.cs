﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Bag myBag = new Bag();
            CraftingTable table = new CraftingTable();
            bool KeepPlaying = true;
            while (KeepPlaying)
            {
                myBag.DisplayItems();
                string Item1 = myBag.GetItem();
                string Item2 = myBag.GetItem();
                string NewItem = table.Combine(Item1, Item2);
                myBag.AddItem(NewItem);
                if (Item1 == "really quit" || Item2 == "really quit")
                {
                    KeepPlaying = false;
                }
            }
            Console.WriteLine("Thanks for playing!");
            Console.Read();
        }
    }
}
