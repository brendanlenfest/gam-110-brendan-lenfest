﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTT
{
    class Program
    {
        static void Main(string[] args)
        {
            Board game1 = new Board();

            bool keepPlaying = true;

            while (keepPlaying)
            {
                game1.DrawBoard();
                game1.TakeTurn();
                if (game1.CheckWin() != '_')
                {
                    game1.DrawBoard();
                    Console.WriteLine("win");
                    keepPlaying = false;
                }

                game1.SwapPlayer();
            }
        }
    }
}
