﻿
using System;

namespace TTT
{
    internal class Board
    {
        
        char[,] cells;
        char currentplayer;
        char Player1;
        char Player2;
        char blank = '_';
        
        public Board()
        {
            cells = new char[3, 3];
            FillCells();
            Player1 = 'X';
            Player2 = 'O';
            currentplayer = Player1;
        }

        private void FillCells()
        {
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    cells[row, col] = blank;
                }
            }
        }



        
        public void DrawBoard()
        {
            Console.Clear();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    string DisplayChar = "|" + cells[col, row] + "|";
                    Console.Write(DisplayChar);
                }
                Console.WriteLine($"  |{row * 3 + 1}||{row * 3 + 2}||{row * 3 + 3}|");
            }
        }

        internal void SwapPlayer()
        {
            if (currentplayer == Player1)
            {
                currentplayer = Player2;
            }
            else
            {
                currentplayer = Player1;
            }
        }

        internal char CheckWin()
        {
            char winner = blank;
            if (cells[0, 0] == Player1 && cells[0, 1] == Player1 && cells[0, 2] == Player1)
            {
                winner = Player1;
            }
            return winner;
        }

        internal void TakeTurn()
        {
            bool validmove = false;
            while (!validmove)
            {
                Console.WriteLine("Player " + currentplayer + " it's your move");
                string input = Console.ReadLine();
                int choice = int.Parse(input);
                int col = (choice - 1) / 3;
                int row = (choice - 1) % 3;
                if (cells[row, col] == blank)
                {
                    cells[row, col] = currentplayer;
                    validmove = true;
                }
            }


        }
    }
}